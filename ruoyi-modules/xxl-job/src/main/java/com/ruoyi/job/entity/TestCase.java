package com.ruoyi.job.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

/**
 * 测试用例对象 test_case
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestCase  {

    /**
     * id主键
     */
    private Integer id;

    /**
     * 删除标识（0正常 1关闭）
     */
    private String delFlag;

    /**
     * 主题
     */
    private String theme;

    /**
     * 类型
     */
    private String type;

    /**
     * 父级任务
     */
    private String parentTask;

    /**
     * 经办人
     */
    private String agent;

    /**
     * 优先级
     */
    private String priority;

    /**
     * 状态
     */
    private String status;

    /**
     * 解决结果
     */
    private String solveResults;

    /**
     * 模块
     */
    private String module;

    /**
     * 期望完成时间
     */
    private Date expectedCompletionTime;

    /**
     * 原估时间
     */
    private String originalEstimateTime;

    /**
     * 标签
     */
    private String label;




}
