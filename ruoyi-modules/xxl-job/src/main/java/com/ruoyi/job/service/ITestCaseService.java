package com.ruoyi.job.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.job.entity.TestCase;

import java.util.List;

/**
 * 测试用例Service接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface ITestCaseService extends IService<TestCase> {





}
